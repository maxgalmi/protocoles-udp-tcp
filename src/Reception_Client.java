import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;

public class Reception_Client extends Thread {
	//private BufferedReader in;
	private DataInputStream in;

	Socket s;
	String message;
	ArrayList<Ad> ads;
	public Reception_Client(Socket s, ArrayList<Ad> ads){
		this.s =s;
		this.ads = ads;
		try {
			in = new DataInputStream(s.getInputStream());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
		
	
	public String[] split(String msg, String delimitor){
		String[] data = msg.split(delimitor);
		for (int i =0; i<data.length; i++)
			data[i] = data[i].trim();
		return data;
	}
	
	
		
	public void run() {
		try {
			while(!s.isClosed()){
				message = in.readUTF();
		       String[] data = split(message, "\\r\\n");
               System.out.println("Message du serveur : " + Arrays.toString(data));
		       if (data.length>=6){
		    	   if(data[0].equals("AD")&&data[1].equals("SEND")){
		    		   System.out.println("Nouvelle annonce :");
		    		   for(int i =2; i < 6; i++)
		    			   System.out.println(data[i]);
		    		 String[] data1 = split(data[2]," ");
		    		   int id = Integer.parseInt(data1[1]);
		    		   data1 = split(data[3]," ");
		    		   String ip = data1[1]; //.split("/")[0];
                       if (ip.charAt(0)=='/')
                           ip = ip.split("/")[1];
		    		   data1 = split(data[4]," ");
		    		   int port = Integer.parseInt(data1[1]);
		    		   data1 = split(data[5]," ");
		    		   String msg = "";
		    		   for(int i =1; i < data1.length;i++)
			 			   msg += data1[i] + " ";
		    		   System.out.println("ID " + id + " IP " + ip + " port " + port + " msg " + msg);
		    		  ads.add(new Ad(id,InetAddress.getByName(ip),port,msg));
		    	   }
		       }else if(data.length==3){
		    	   if(data[0].equals("AD")&&data[1].equals("DEL")){
		    		   System.out.println("Annonce supprimee :");
		    		   System.out.println(data[2]);
		    		   String[] data1 = split(data[2]," ");
		    		   int id = Integer.parseInt(data1[1]);
		    		   for(int i=0; i<ads.size();i++){
		    			   if (ads.get(i).id == id){
		    				   ads.remove(i);
		    			   }
		    		   }
		    	   }
		       }/*else{
		    	   System.out.println("Message du serveur : " + message);
		       }*/
		    	   
			    
			}
		} catch (IOException e) {
			
	    	System.out.println("Deconnecte");
	    	System.exit(0);
	    	
		}
	}

}
	
	
	
