import java.net.InetAddress;
import java.util.ArrayList;

public class Adlist {
	int current_id; //correspond au nombre total d'annonces postes
	ArrayList<Ad> list;
	
	public Adlist(){
		current_id = 0;
		list = new ArrayList<Ad>();
	}
	
	public Adlist(Adlist l){
		current_id = l.getId();
		list = new ArrayList<Ad>();
		for (int i=0; i< l.getList().size();i++){
			list.add(l.getList().get(i));
		}
	}
	
	public synchronized int add(InetAddress ip, int port, String msg){
		
		list.add(new Ad(++current_id,ip,port,msg));
		System.out.println("Annonce ajoute ID :" + current_id + " MSG " + msg);
		return current_id;
		
	}
	
	public Ad getAd(int id){
		for (int i =0; i < list.size(); i++){
			if (list.get(i).id==i)
				return list.get(i);
		}
		return null;
	}
	
	
	public int getId(){
		return current_id;
	}
	
	public ArrayList<Ad> getList(){
		return list;	
	}
	
	public ArrayList<Ad> added_ads(Adlist old){ //ancien current_id
		ArrayList<Ad> added = new ArrayList<Ad>();
		for(int i = 0; i <list.size();i++){
			if (list.get(i).id > old.getId()){
				added.add(list.get(i));
			}
		}	
		return added;	
	}
	
	public ArrayList<Ad> deleted_ads(Adlist old){
		ArrayList<Ad> deleted = new ArrayList<Ad>();
		boolean trouve=false;
		for(int i = 0; i <old.getList().size();i++){
			/*if (getAd(old.getList().get(i).id)==null){
				
			}*/
			for(int j =0; j<list.size();j++){
				if(old.getList().get(i).equals(list.get(j))){
					trouve = true; break;
				}
			}if (!trouve){
				deleted.add(old.getList().get(i));
				
			}trouve=false;
			
		}
		
		return deleted;	
	}
	
	public boolean delete_ad(int id, InetAddress ip, int port){
		for (int i =0; i < list.size(); i++){
			if (list.get(i).id==id&&list.get(i).ip.toString().equals(ip.toString())&&list.get(i).port==port){
				list.remove(i);
				return true;
			}
		}return false;
	}
	
	public void delete_ads(InetAddress ip, int port){
		for (int i =0; i < list.size(); i++){
			if (list.get(i).ip.equals(ip)&&list.get(i).port==port){
				System.out.println("Annonce supprimee " + list.get(i).id);
				list.remove(i); i--;
			}
		}
	}
}

