import java.io.IOException;
import java.net.DatagramSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;

public class Client  {
	
	
	public static void main(String[] args){
		int port = (int) (Math.random() * 10000 + 1028);
		Socket s;
		Boolean arret = new Boolean(false);
		 ArrayList<ChatUDP> chat = new ArrayList<ChatUDP>();
		 
		try {
			ArrayList<Ad> ad = new ArrayList<Ad>();
			s = new Socket("localhost",1027);
			System.out.println("Connexion etablie avec le serveur, authentification");
			System.out.println("Mon port : "+ port);
			DatagramSocket clientSocket = new DatagramSocket(port);
			new Reception_Client(s,ad).start();
			new Emission_Client(s, port,ad,arret, chat, clientSocket).start();
			new Reception_ClientUDP(chat,port,clientSocket,ad, s,arret).start();
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
