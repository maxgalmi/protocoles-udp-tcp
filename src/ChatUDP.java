import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class ChatUDP implements ActionListener {

      //private static UDPCS1 chat = new UDPCS1();
      private final JFrame f = new JFrame();
      private final JTextField tf = new JTextField(60);
      private final JTextArea textArea = new JTextArea(10, 60);
      private final JButton send = new JButton("Send");
      private final JMenuBar bar = new JMenuBar();
      private final JMenu menu = new JMenu("Annonce");
     
      private final JMenuItem conclure = new JMenuItem("Conclure transaction");
      int port;
   
      InetAddress ip ;
      int id_ad;
      byte[] sendData;
      DatagramSocket clientSocket;
      public ChatUDP(int port, InetAddress ip, DatagramSocket clientSocket, final int id_ad,final ArrayList<Ad> ads) {
    	  this.port = port;
    	  this.ip = ip;
    	  this.id_ad = id_ad;
    	  sendData = new byte[1024];
    	  this.clientSocket = clientSocket;
          f.setTitle("Chat " + ip.toString() + " port " + port + " ID-annonce " + id_ad);
          f.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
          f.getRootPane().setDefaultButton(send);
          f.add(tf, BorderLayout.CENTER);
          f.add(new JScrollPane(textArea), BorderLayout.NORTH);
          f.add(send, BorderLayout.EAST);
          f.setJMenuBar(bar);
          bar.add(menu);
          menu.add(conclure);
          conclure.addActionListener(this);
          f.setLocation(400, 300);
          f.pack();
          send.addActionListener(this);
          textArea.setLineWrap(true);
          textArea.setWrapStyleWord(true);
          textArea.setEditable(false);
          getF().setVisible(true);
          
          Thread one = new Thread() {
        	    public void run() {
        	    boolean found = true;
        	    while(found)
        	        try {
        	        	boolean found2 = false;
        	           for(int i = 0; i< ads.size(); i++){
        	        	   if (ads.get(i).id==id_ad) found2 =true;
        	           }found = found2;
        	           if(!found){
        	        	   textArea.append("Annonce supprimee" + System.getProperty("line.separator"));
        	        	   tf.setEditable(false);
        	        	   conclure.setEnabled(false);
        	        	   
        	           }
        	           sleep(2000);
        	        } catch(InterruptedException v) {
        	            System.out.println(v);
        	        }
        	    }  
        	};

        	one.start();
      }

      @Override
      public void actionPerformed(ActionEvent event) {
    	  try {
    	  if(event.getActionCommand().equals("Send")&&!tf.getText().isEmpty()){
    		  String msg = tf.getText();
    	       
    	      textArea.append("Votre message : " + msg + System.getProperty("line.separator"));
    	        textArea.setCaretPosition(textArea.getDocument().getLength());
    	     tf.setText("");
    	     msg = "AD \r\n ID " + id_ad +" \r\n MSG " + msg +" \r\n";
    	     sendData = msg.trim().getBytes();
 			DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, ip, port);
 			
				clientSocket.send(sendPacket);
			    
    		  
    	  }else if(event.getActionCommand().equals("Conclure transaction")){
    		  display("Vous avez demande a conclure la transaction.");
    		  
    		  String msg = "AD \r\n QUERY \r\n ID " + id_ad + " \r\n";
    		  sendData = msg.trim().getBytes();
    		  DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, ip, port);
    		  clientSocket.send(sendPacket);
    	  }sendData = new byte[1024];
    	  } catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		}   
      }
      
      public void display(String msg){
    	  f.setVisible(true);
    	  textArea.append( msg + System.getProperty("line.separator"));
          textArea.setCaretPosition(textArea.getDocument().getLength());
      }

      
      public static void main(String args[]){
    	  
    	  ArrayList<ChatUDP> chat = new ArrayList<ChatUDP>();
    	  chat.add(new ChatUDP(13,null,null,1,null));
    	  
    	  while(true){
    		  try {
				Thread.sleep(5000);
				chat.get(0).getF().setVisible(true);
				chat.get(0).display("50");
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	  }
      }

	public JFrame getF() {
		return f;
	}


      
}