import java.io.DataOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;


public class Emission_Serveur extends Thread{
	
	Socket s;
	DataOutputStream out;
	Adlist list;
	Adlist old_list;
	String message ="";
	//int port;
	public Emission_Serveur(Socket s, Adlist l){
		this.s = s;
		list = l;
		old_list = new Adlist();
		//this.port = port;
		try {
			out =new DataOutputStream(s.getOutputStream());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
    
    public String[] split(String msg, String delimitor){
		String[] data = msg.split(delimitor);
		for (int i =0; i<data.length; i++)
			data[i] = data[i].trim();
		return data;
	}
	
	public void write(String message){
		try {
			out.writeUTF(message);
			out.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
	}
	
	public void run(){
		
		System.out.println("list size " + list.list.size());
		/*for (int i = 0; i < list.getList().size();i++){
    		message = "AD \r\n SEND \r\n ID " + list.getList().get(i).id +
    				" \r\n IPv4 " + list.getList().get(i).ip +
    				" \r\n PORT "+ list.getList().get(i).port
    				+ " \r\n MSG " + list.getList().get(i).msg + " \r\n";
    		//System.out.println("message :" + message);
    		write(message);
    	}
		*/
		while(!s.isClosed()){
			
            try {
            	
            	ArrayList<Ad> added = list.added_ads(old_list);
            	ArrayList<Ad> deleted = list.deleted_ads(old_list);
            	/*System.out.println("old " + old_list.getList().size());
            	System.out.println("ajoute " + added.size() );
            	System.out.println("supprime " + deleted.size() );*/
            	old_list = new Adlist(list);
            	for (int i = 0; i < added.size();i++){
            		message = "AD \r\n SEND \r\n ID " + added.get(i).id +
            				" \r\n IPv4 " + added.get(i).ip.toString().split("/")[1] +
            				" \r\n PORT "+ added.get(i).port
            				+ " \r\n MSG " + added.get(i).msg + " \r\n";
            		//System.out.println("message :" + message);
            		write(message);
            	}
            	
            	for (int i = 0; i < deleted.size();i++){
            		message = "AD \r\n DEL \r\n ID " + deleted.get(i).id;
            		write(message);
            	}
				sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	

}
