import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.util.Scanner;

import javax.swing.JOptionPane;

public class Emission_Client extends Thread {
	private DataOutputStream out;
	Socket s;
	int port; //port UDP client-client
	String  message = "";
	private Scanner sc = null;
	ArrayList<Ad> ads;
	Boolean arret;
	 ArrayList<ChatUDP> chat;
	 DatagramSocket clientSocket;
	public Emission_Client(Socket s, int port, ArrayList<Ad> ads, Boolean arret, ArrayList<ChatUDP> chat,DatagramSocket clientSocket) {
		// TODO Auto-generated constructor stub
		this.s = s;
		this.port = port;
		this.ads = ads;
		this.arret = arret;
		this.chat = chat;
		this.clientSocket = clientSocket;
		try {
			out =new DataOutputStream(s.getOutputStream());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void menu(){
		System.out.println("------------------------------------");
		System.out.println("1 - Ajouter annonce");
		System.out.println("2 - Supprimer annonce");
		System.out.println("3 - Voir liste annonce");
		System.out.println("4 - Contacter a propos d'une annonce");
		System.out.println("0 - Se deconnecter");
		System.out.println("------------------------------------");
		System.out.println("Entrer un nombre : ");
	}

	public void write(String message){
		try {
			out.writeUTF(message);
			out.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}    
	}
	public void run(){
		sc = new Scanner(System.in);
		message =  "USR \r\n PORT " + port + " \r\n";
	//	System.out.println(message);
		write(message);
        while(!s.isClosed()){
        	
        	int n = -1;
        	while(n<0||n>4){
        	
        		menu();
        		try{
        			n = sc.nextInt();
        		}catch(InputMismatchException e){sc = new Scanner(System.in);}
        		catch(NoSuchElementException e){sc = new Scanner(System.in);}
        	}
        	
        	if (n==1){
        		System.out.println("Entrez annonce :");
        		String annonce = sc.nextLine();
        		annonce = sc.nextLine();
        		message = "AD \r\n ADD \r\n MSG " + annonce + " \r\n";
        		write(message);
        		
        	}else if(n==2){
        		System.out.println("Entrez l'id de l'annonce a supprimer :");
        		try{
        			int id = sc.nextInt();
        			if(id<1){ 
        				System.out.println("Id invalide.");
        			}else{
        				message = "AD \r\n DEL \r\n  ID " + id + "\r\n";
        				write(message);
        			}
        		}catch(InputMismatchException e){System.out.println("Id invalide.");}
        		
        	}else if (n == 3){
        		String annonces= "";
        		for (int i = 0; i< ads.size();i++)
        			annonces += ads.get(i).toString() + System.getProperty("line.separator");
        		JOptionPane.showMessageDialog(null,annonces); 
        	}else if (n == 4){
        		System.out.println("Entrez l'id de l'annonce:");
        		try{
        			int id = sc.nextInt();
        			if(id<1){ 
        				System.out.println("Id invalide.");
        			}else{
        				for (int i = 0; i < ads.size();i++){
        					if(ads.get(i).id==id&&(ads.get(i).port!=port||ads.get(i).ip.equals(InetAddress.getLocalHost()))){
        						boolean found = false;
        						 for (int k = 0; k <chat.size()&&!found;k++){
        				 				if (ads.get(i).port == chat.get(k).port&& ads.get(i).ip.toString().equals(chat.get(k).ip.toString())
        				 						&& id== chat.get(k).id_ad){
        				 					found = true;
        				 					
        				 					chat.get(k).getF().setVisible(true);
        				 				}
        				 		}
        						if(!found) chat.add(new ChatUDP(ads.get(i).port,ads.get(i).ip,clientSocket,id, ads));
        						break;
        					}
        				}
        			}
        		}catch(InputMismatchException e){
        			System.out.println("Id invalide.");
        		} catch (UnknownHostException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
        		
        	}else if (n == 0){
        		message = "DISCONNECT \r\n";
        		write(message);
        	}
              

        	//System.out.println(message);
              if(message.equals("DISCONNECT \r\n")){
				try {
					arret = true;
					s.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
              }

         }
	}

}
