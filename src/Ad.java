import java.net.InetAddress;

public class Ad {
	int id;
	InetAddress ip;
	int port;
	String msg;
	
	public Ad(int id, InetAddress ip, int port, String msg){
		this.id = id;
		this.ip = ip;
		this.port = port;
		this.msg = msg;
	}
	public String toString(){
		
		return "ID : " + id + System.getProperty("line.separator")
		+ "IP : " + ip.toString() + System.getProperty("line.separator")
		+ "Port : " + port + System.getProperty("line.separator")
		+ "Message : " + msg;
		
	}
}
