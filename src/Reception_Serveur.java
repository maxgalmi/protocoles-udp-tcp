import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Arrays;

public class Reception_Serveur extends Thread{
	Socket s;
	Adlist list;
	InetAddress ip;
	private DataInputStream in;
	String message = "";
	int port=0; //port client
	DataOutputStream out;
	boolean arret = false;
	
	public Reception_Serveur(Socket s,  Adlist list){
		this.s = s;
		this.list = list;
		ip = s.getInetAddress();
		try {
			in = new DataInputStream(s.getInputStream());
			out =new DataOutputStream(s.getOutputStream());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public String[] readLine(){
		try {
			message = in.readUTF();
			System.out.println("Message du client : " + message);
			
		} catch (IOException e) {
			System.out.println("DECONNEXION");
			deconnexion();
		}
		  
		return split(message, "\\r\\n");
	}
	public String[] split(String msg, String delimitor){
		String[] data = msg.split(delimitor);
		for (int i =0; i<data.length; i++)
			data[i] = data[i].trim();
		return data;
	}
	
	public void send(String message){
		System.out.println("Message : ");
		try {
			out.writeUTF(message);
			out.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void deconnexion(){
		arret=true;
		list.delete_ads(ip, port);
		try {
			s.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void run(){
		
			String[] data = readLine();
			 System.out.println(Arrays.toString(data));
			if (data.length!=2){
				send("SYS \r\n ERROR error \r\n error = 001 Bad Request");
			}else if(data[0].equals("USR")){
				//System.out.println("debug1");
				String[] data1 = split(data[1]," ");
				// System.out.println("data1 " +Arrays.toString(data));
				if(data1.length==2&&data1[0].equals("PORT")){
					try{
					 port = Integer.parseInt(data1[1]);
					 System.out.println("port " + port);
					}catch(NumberFormatException e){
						send("SYS \r\n ERROR error \r\n error = 001 Bad Request");
					}
				}	
			}
		
		if(port!=0){
			while(!s.isClosed()&&!arret){
			//	System.out.println("debug ");
				data = readLine();
				
				if(data.length==3){
				//	System.out.println("debug1 ");
					if(data[0].equals("AD")){
						//System.out.println("debug2 ");
						if(data[1].equals("ADD")){
							String[] data1 = split(data[2]," ");
                            if (data1[0].equals("MSG")){
                                String ad = "";
                                for (int i =1; i<data1.length;i++)
                                    ad += data1[i] + " ";
                                System.out.println("AJOUT");
                               if(!arret) list.add(ip, port, ad);
                           }     
							
						}else if (data[1].equals("DEL")){
							String[] data1 = split(data[2]," ");
							if(data1.length==2&&data1[0].equals("ID")){
								try{
									 int id = Integer.parseInt(data1[1]);
									 System.out.println("id " + id);
									 System.out.println(list.delete_ad(id, ip, port));
									}catch(NumberFormatException e){
										send("SYS \r\n ERROR error \r\n error = 001 Bad Request");
									}
							}else send("SYS \r\n ERROR error \r\n error = 001 Bad Request");
						}
					}else send("SYS \r\n ERROR error \r\n error = 001 Bad Request");
				}else if(data.length==1){
					if (data[0].equals("DISCONNECT")){
						deconnexion();
					}
				}
				
				
				
			}
		}
		
	}

}
