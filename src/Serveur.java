import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Serveur extends Thread {
	Socket socket;
	final static int port = 1027;
	Adlist list;
	public Serveur(Socket s, Adlist l){
		socket = s;
		list = l;
	}
	
	public void run(){
		System.out.println("Client connecte !");
		
		new Reception_Serveur(socket, list).start();
		new Emission_Serveur(socket,list).start();
		
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Lancement du serveur");
		Adlist l = new Adlist();
		ServerSocket ss;
		try {
			ss = new ServerSocket(port);
			while(true){
				Socket s = ss.accept();
				Serveur t = new Serveur(s,l);
				t.start();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	}

}
