import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.Socket;
import java.util.ArrayList;

public class Reception_ClientUDP  extends Thread{
	ArrayList<ChatUDP> chat;
	ArrayList<Ad> ads; 
	int port;
	DatagramSocket clientSocket;
	byte[] receiveData;
	byte[] sendData;
	Boolean arret;
	Socket s;
	public Reception_ClientUDP(ArrayList<ChatUDP> chat, int port,DatagramSocket clientSocket, ArrayList<Ad> ads, Socket s, Boolean arret){
		this.chat = chat;
		this.arret = arret;
		this.ads = ads;
		this.clientSocket = clientSocket;
		this.port = port;
		this.s = s;
		receiveData = new byte[1024];
	}
	
	
	
	public void run(){
		while(!arret){
			DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
			try {
				clientSocket.receive(receivePacket);
				System.out.println("Message recu");
				(new TraitementMessageUDP(receivePacket,chat,clientSocket,ads,port,s)).run();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			receiveData = new byte[1024];
		}
	}
}
