import java.io.DataOutputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class TraitementMessageUDP extends Thread {
	DatagramPacket receivePacket;
	ArrayList<ChatUDP> chat;
	DatagramSocket clientSocket;
	ArrayList<Ad> ads;
	DataOutputStream out;
	int my_port;
	public TraitementMessageUDP(DatagramPacket rp, ArrayList<ChatUDP> chat, DatagramSocket clientSocket,ArrayList<Ad> ads, int my_port,Socket s){
		receivePacket = rp;
		this.chat = chat;
		this.clientSocket = clientSocket;
		this.ads = ads;
		this.my_port = my_port;
		try {
			out =new DataOutputStream(s.getOutputStream());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void write(String message){
		try {
			out.writeUTF(message);
			out.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}    
	}
	
	public String[] split(String msg, String delimitor){
		String[] data = msg.split(delimitor);
		for (int i =0; i<data.length; i++)
			data[i] = data[i].trim();
		return data;
	}
	
	
	public void conclure(int id_ad){
		for(int i = 0; i < ads.size();i++){
			if(ads.get(i).port==my_port&& ads.get(i).id==id_ad){
				String message = "AD \r\n DEL \r\n  ID " + id_ad + "\r\n";
				write(message);
				break;
			}
		}
	}
	public void run(){
		boolean found = false;
		String sentence = new String(receivePacket.getData());
		int port = receivePacket.getPort();
		InetAddress ip = receivePacket.getAddress();
		String[] data = split(sentence, "\\r\\n");
		System.out.println("test");
		System.out.println("Message recu. port : " + port + " ip : " + ip.toString());
	
		
		System.out.println(Arrays.toString(data));
		if(data.length>=3){
			if(data[0].equals("AD")){
				//System.out.println("passe1");
				String[] data1 = split(data[1]," ");
				String[] data2 = split(data[2]," ");
				if(data1[0].equals("ID")&&data2[0].equals("MSG")){
					//System.out.println("passe2");
		 		   int id = Integer.parseInt(data1[1]);
		 		   String msg = "";
		 		   for(int i =1; i < data2.length;i++)
		 			   msg += data2[i] + " ";
		 		  for (int i = 0; i <chat.size()&&!found;i++){
		 				if (port == chat.get(i).port&& ip.toString().equals(chat.get(i).ip.toString())
		 						&& id== chat.get(i).id_ad){
		 					found = true;
		 					
		 					chat.get(i).display("Message du client : " +msg);
		 				}
		 			}if(!found){
		 				ChatUDP c = new ChatUDP(port,ip,clientSocket,id, ads);
		 				c.display("Message du client : " +msg);
		 				chat.add(c);
		 			}
		 		   
				}else if(data[1].equals("QUERY")&&data2[0].equals("ID")){
					try{
					int id = Integer.parseInt(data2[1]);
					JFrame f = null;
					ChatUDP c = null;
					  for (int i = 0; i <chat.size()&&!found;i++){
			 				if (port == chat.get(i).port&& ip.toString().equals(chat.get(i).ip.toString())
			 						&& id== chat.get(i).id_ad){
			 					found = true;
			 					c = chat.get(i);
			 					f = chat.get(i).getF();
			 				}
			 			}if(!found){
			 				c = new ChatUDP(port,ip,clientSocket,id, ads);
			 				f = c.getF();
			 				chat.add(c);
			 			}
			 			f.setVisible(true);
			 			 byte[] sendData = new byte[1024];
			 			if (JOptionPane.showConfirmDialog(f, "Demande de conclusion de transaction. Accepter?", "WARNING",
	 					        JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
                            boolean trouve = false;
                            for(int i =0;i<ads.size()&&!trouve;i++){
                            	if(ads.get(i).id==id) trouve = true;
                            }
                           if(trouve) {
                        	  c.display("Vous avez accepte de conclure la transaction");
     	 					  String msg = "AD \r\n ACCEPT \r\n ID " + id + " \r\n";
     	 		    		  sendData = msg.trim().getBytes();
     	 		    		  DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, ip, port); 
     	 		    		  clientSocket.send(sendPacket);
     	 		    		  conclure(id);
                           }else{
                        	   c.display("Annonce supprimee, transaction annulee.");
                           }
	 					  
	 		    		  
	 					} else {
	 						 c.display("Vous avez refuse de conclure la transaction");
	 						 String msg = "AD \r\n REFUSE \r\n ID " + id + " \r\n";
		 		    		  sendData = msg.trim().getBytes();
		 		    		 DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, ip, port);
		 		    		  clientSocket.send(sendPacket);
	 					}
					}catch(NumberFormatException e){
						System.out.println("Erreur");
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			 		   
				}else if(data[1].equals("REFUSE")&&data2[0].equals("ID")){
					
						int id = Integer.parseInt(data2[1]);
						  for (int i = 0; i <chat.size()&&!found;i++){
				 				if (port == chat.get(i).port&& ip.toString().equals(chat.get(i).ip.toString())
				 						&& id== chat.get(i).id_ad){
				 					found = true;
				 					chat.get(i).display("Transaction refusee");
				 					
				 				}
				 			}
				}else if(data[1].equals("ACCEPT")&&data2[0].equals("ID")){
					
					int id = Integer.parseInt(data2[1]);
					  for (int i = 0; i <chat.size()&&!found;i++){
			 				if (port == chat.get(i).port&& ip.toString().equals(chat.get(i).ip.toString())
			 						&& id== chat.get(i).id_ad){
			 					found = true;
			 					chat.get(i).display("Transaction acceptee");
			 					conclure(id);
			 					
			 				}
			 			}
			}
			}
			
		}
		
	}
	

}
